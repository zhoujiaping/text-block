package org.sirenia.textblock;


import java.io.*;

public class Texts {
    private static final String START_COMMENT = "/*";
    private static final String END_COMMENT = "*/";
    private static final String LINE_SEPARATOR = System.lineSeparator();

    public static String text(/*TextBlock*/) {
        return text0();
    }
    private static String text0() {
        StackTraceElement[] elements = new Exception().getStackTrace();
        StackTraceElement ele = elements[2];
        String clazzName = ele.getClassName().split("\\$")[0];
        final String resource = clazzName.replace('.', '/') + ".java";
        InputStream in = null;
        try {
            in = Class.forName(clazzName).getClassLoader().getResourceAsStream(resource);
            if (in == null) {
                throw new FileNotFoundException("Resource '" + resource + "' not found in classpath");
            }
            return readText(in, ele.getLineNumber());
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static String readText(InputStream is, int lineNum) throws IOException {
        LineNumberReader reader = new LineNumberReader(new InputStreamReader(is));
        String line = reader.readLine();
        StringBuilder text = new StringBuilder();
        int startLineNum = -1;
        for (; line != null; line = reader.readLine()) {
            if(reader.getLineNumber()<lineNum){
                continue;
            }
            if(startLineNum<0 && line.contains(START_COMMENT)){
                startLineNum = reader.getLineNumber();
                text.append(line.substring(line.indexOf(START_COMMENT)+START_COMMENT.length())).append(LINE_SEPARATOR);
            }else if(line.contains(END_COMMENT)){
                text.append(line, 0, line.indexOf(END_COMMENT));
                break;
            }else if(startLineNum>=0){
                text.append(line).append(LINE_SEPARATOR);
            }
        }
        if(startLineNum<0){
            throw new RuntimeException("textBlock not found at line "+lineNum);
        }
        return text.toString();
    }
}
