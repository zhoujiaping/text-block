package org.sirenia.textblock.watch;

import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class FileListenerBootstrap {
    private static Logger log = LoggerFactory.getLogger(FileListenerBootstrap.class);

    public static void main(String[] args) throws Exception {
        Config config = new Config();
        new FileListenerBootstrap().start(config);
        log.info("fileListenerBootstrap started. config={}", config);
    }

    public void start(Config config) throws Exception {
        // 创建一个文件观察器用于处理文件的格式
        File mainJavaFile = new File(config.projectBaseDir, config.mainJavaDir);
        IOFileFilter filter = FileFilterUtils.or(FileFilterUtils.directoryFileFilter(),
                FileFilterUtils.and(
                        FileFilterUtils.fileFileFilter(),
                        FileFilterUtils.suffixFileFilter(".java")
                )
        );
        FileAlterationObserver mainJavaObserver = new FileAlterationObserver(
                mainJavaFile,
                filter,
                null);
        mainJavaObserver.addListener(new JavaSourceFileListener(config.projectBaseDir + config.mainJavaDir,
                config.projectBaseDir + config.mainClassesDir)); //设置文件变化监听器
        log.info("监听{}", mainJavaFile);
        // 创建一个文件观察器用于处理文件的格式
        File testJavaFile = new File(config.projectBaseDir, config.testJavaDir);
        FileAlterationObserver testJavaObserver = new FileAlterationObserver(
                testJavaFile,
                filter,
                null);
        log.info("监听{}", testJavaFile);
        testJavaObserver.addListener(new JavaSourceFileListener(config.projectBaseDir + config.testJavaDir,
                config.projectBaseDir + config.testClassesDir)); //设置文件变化监听器
        //创建文件变化监听器
        FileAlterationMonitor monitor = new FileAlterationMonitor(config.interval, testJavaObserver);
        // 开始监控
        monitor.start();
    }
}
