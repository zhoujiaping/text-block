package org.sirenia.textblock.watch;

import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class JavaSourceFileListener extends FileAlterationListenerAdaptor {
    private static Logger log = LoggerFactory.getLogger(JavaSourceFileListener.class);
    private String fromDir;
    private String toDir;

    public JavaSourceFileListener(String fromDir, String toDir) {
        this.fromDir = normalizePath(fromDir);
        this.toDir = normalizePath(toDir);
    }

    public void onFileCreate(File file) {
        copy(file);
    }

    public void onFileChange(File file) {
        copy(file);
    }

    public void onFileDelete(File file) {
        String path = normalizePath(file.getAbsolutePath());
        String relativePath = path.replace(fromDir, "");
        try {
            Path target = Paths.get(toDir, relativePath);
            log.info("delete {}", target);
            if(target.toFile().exists()) {
                Files.delete(target);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void copy(File file) {
        String path = normalizePath(file.getAbsolutePath());
        String relativePath = path.replace(fromDir, "");
        try {
            Path target = Paths.get(toDir, relativePath);
            log.info("copy {} => {}", file, target);
            Files.copy(file.toPath(), target, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String normalizePath(String path) {
        return path.replaceAll("(\\\\|/)+", "/");
    }

}
