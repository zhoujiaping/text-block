package org.sirenia.textblock.watch;

public class Config {
    public String projectBaseDir = System.getProperty("user.dir");
    public String mainJavaDir = "/src/main/java";
    public String testJavaDir = "/src/test/java";
    public String mainClassesDir = "/target/classes";
    public String testClassesDir = "/target/test-classes";
    /**文件监听线程扫描间隔，单位ms*/
    public long interval = 2000;
    @Override
    public String toString(){
        return "projectBaseDir="+projectBaseDir+",mainJavaDir="+mainClassesDir
                +",testJavaDir="+testJavaDir+",mainClassesDir="+mainClassesDir
                +",testClassesDir="+testClassesDir;
    }
}
