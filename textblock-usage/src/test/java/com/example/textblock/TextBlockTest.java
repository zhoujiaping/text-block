package com.example.textblock;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.sirenia.textblock.demo.Demo;

import static org.sirenia.textblock.Texts.text;

public class TextBlockTest {
    @Test
    public void testTextBlock(){
        System.out.println(text(/*
        In a previous tutorial,
        we saw how we can use multi-line strings in any Java version.
        In this tutorial, we'll see in detail how to use the Java 15 text blocks feature
        to declare multi-line strings most efficiently.
        */));
    }
    @Test
    public void testTextBlockInJar(){
        Assertions.assertTrue(Demo.echo().contains("this is a textBlock test"));
    }
}
