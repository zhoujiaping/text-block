## gradle
gradle项目可以为编译任务添加动作，修改源码后会自动执行拷贝源码。
```
compileTestJava{
    doLast{
        copy{
            from 'src/main/java'
            into '$buildDir/resources/main'
        }
        copy{
            from 'src/test/java'
            into '$buildDir/resources/test'
        }
    }
}
```

## maven
### develop
maven项目貌似没有修改源码后自动触发的动作，需要手动执行compile才能触发antrun插件执行。
所以在开发环境，通过监听文件的方式，实现源码同步到classes和test-classes。
（考虑过开发idea插件，但是比较麻烦，而且不同idea版本插件开发不太一样。
而且不支持其他开发工具。）
```
//启动文件监听
public static void main(String[] args) throws Exception {
    Config config = new Config();
    new FileListenerBootstrap().start(config);
    log.info("fileListenerBootstrap started. config={}",config);
}
```
### dependency
### plugin
```
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-antrun-plugin</artifactId>
    <version>3.0.0</version>
    <executions>
        <execution>
            <id>compile</id>
            <phase>compile</phase>
            <goals>
                <goal>run</goal>
            </goals>
            <configuration>
                <target>
                    <copy todir="${project.basedir}/target/classes">
                        <fileset dir="${project.basedir}/src/main/java">
                            <include name="**/*.java" />
                        </fileset>
                    </copy>
                    <copy todir="${project.basedir}/target/test-classes">
                        <fileset dir="${project.basedir}/src/test/java">
                            <include name="**/*.java" />
                        </fileset>
                    </copy>
                </target>
            </configuration>
        </execution>
    </executions>
</plugin>
```